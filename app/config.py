from pydantic import BaseSettings


class Settings(BaseSettings):
    PG_PASS: str
    PG_USER: str
    PG_DB: str
    PG_HOST: str
    PG_PORT: str

    JWT_PUBLIC_KEY: str
    JWT_PRIVATE_KEY: str
    REFRESH_TOKEN_EXPIRES_IN: int
    ACCESS_TOKEN_EXPIRES_IN: int
    JWT_ALGORITHM: str

    class Config:
        env_file = './.env'


settings = Settings()
