from fastapi import APIRouter, Depends, status, HTTPException, Query
from sqlalchemy.orm import Session

from app import models, oauth2
from app.database import get_db
from app.schemas import PostModel

router = APIRouter()


@router.get('/')
def get_posts(db: Session = Depends(get_db),
              user_id: str = Depends(oauth2.require_user),
              pages: int = Query(ge=1)):
    max_per_page = 10
    offset = (pages - 1) * max_per_page

    posts = db.query(models.Post).join(models.User).filter(
        models.User.id == user_id
    ).limit(max_per_page).offset(offset).all()

    return posts


@router.post('/', status_code=status.HTTP_200_OK, response_model=PostModel)
def create_post(payload: PostModel, user_id: str = Depends(oauth2.require_user), db: Session = Depends(get_db)):
    if len(payload.description) < 1:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail='Post cannot be empty')

    new_post = models.Post(
        user_id=user_id,
        description=payload.description
    )
    db.add(new_post)
    db.commit()
    db.refresh(new_post)

    return new_post


@router.put('/{post_id}', status_code=status.HTTP_200_OK, response_model=PostModel)
def update_post(post_id: int, payload: PostModel, user_id: str = Depends(oauth2.require_user), db: Session = Depends(get_db)):
    if len(payload.description) < 1:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail='Post cannot be empty')

    post_to_update = db.query(models.Post).filter(
        models.Post.id == post_id
    ).first()

    if not post_to_update or (str(post_to_update.user_id) != user_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail='Post not found')

    post_to_update.description = payload.description
    db.add(post_to_update)
    db.commit()
    db.refresh(post_to_update)

    return post_to_update


@router.delete('/{post_id}', status_code=status.HTTP_200_OK)
def delete_post(post_id: int, user_id: str = Depends(oauth2.require_user), db: Session = Depends(get_db)):
    post_to_delete = db.query(models.Post).filter(
        models.Post.id == post_id
    ).first()

    if not post_to_delete or (str(post_to_delete.user_id) != user_id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail='Post not found')

    db.delete(post_to_delete)
    db.commit()

    return {"message": "Delete successful"}
