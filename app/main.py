from fastapi import FastAPI
from app.router import posts, auth

app = FastAPI()

app.include_router(auth.router, tags=['Auth'], prefix='/auth')
app.include_router(posts.router, tags=['Posts'], prefix='/posts')


@app.get("/healthchecker")
async def root():
    return {"message": "Hello World"}
