from typing import Union, List

from pydantic import BaseModel, Field


class PostModel(BaseModel):
    _id: int
    description: str

    class Config:
        orm_mode = True
