from pydantic import BaseModel, constr


class UserBaseSchema(BaseModel):
    username: str

    class Config:
        orm_mode = True


class UserSchema(UserBaseSchema):
    password: constr(min_length=6)
