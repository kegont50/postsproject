FROM python:3.10
WORKDIR /PostsProject
COPY . /PostsProject
RUN pip install --upgrade -r requirements.txt
RUN chmod +x ./docker-entrypoint.sh
ENTRYPOINT ["./docker-entrypoint.sh"]